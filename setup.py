from distutils.core import setup

setup(
    name='pynetopt',
    version='0.0.1',
    author='J. McGehee',
    author_email='jlmcgehee21@gmail.com',
    packages=['pythonChroma','pythonChroma.Pro'],
#     scripts=['bin/stowe-towels.py','bin/wash-towels.py'],
#     url='http://pypi.python.org/pypi/TowelStuff/',
    license='LICENSE.txt',
    description='Package for parallel simulations of Pybrain Neural Networks',
    long_description=open('README.txt').read(),
    install_requires=[
        "matplotlib >= 1.3, <2.0",
        "numpy >= 1.8, <2.0",
        "PyBrain == 0.3",
        "scipy >= 0.13.0",
    ],
)