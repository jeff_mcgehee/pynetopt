__author__ = 'Jeff'

import simulations

import multiprocessing
import numpy as np

from pybrain import datasets
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.supervised.trainers import RPropMinusTrainer
from pybrain.structure import LinearLayer
from pybrain.structure import TanhLayer
from pybrain.structure import SigmoidLayer
from pybrain.structure import SoftmaxLayer

class Simulation(simulations.Simulation):
    def __init__(self, inputs, targets, nets_properties, dataset_properties, trainers_properties, custom_error=None):

        super(Simulation, self).__init__(inputs, targets, nets_properties, dataset_properties, trainers_properties,
                                         custom_error)


    def run(self, epochs, decimation=1,save_nets=False):
        """
        Trains multiple networks in parallel (for different chroma devices) towards a single target set.
        :return:
        """

        #FIXME: Add saved nets

        # trainData = self.dataset


        # print(trainData.indim,trainData.outdim)
        # print(trainData.getLength())


        allNets = []
        # allNames = []
        for ind in range(len(self._weights)):
            # newSet = trainData


            # newTrainer = self.trainers[ind]


            allNets.append((ind, epochs, decimation))

            # #FIXME: Add names
            # allNames.append('netOptTest')


        num_processors = multiprocessing.cpu_count()

        tasks = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()

        processors = [multiprocessing.Process(target=self._train_single, args=(tasks, results))
                      for i in xrange(num_processors)]

        for processor in processors:
            print('starting processor..')
            processor.start()


        try:

            numANNs = len(allNets)

            for chromaNet in allNets:
                print('putting chromaNets')
                tasks.put(chromaNet)

            outputs = []
            for i in range(numANNs):
                resultVal = results.get()
                if resultVal is KeyboardInterrupt:
                    for i in xrange(num_processors):
                        tasks.put(None)

                    tasks.join()
                    raise KeyboardInterrupt
                else:
                    outputs.append(resultVal)

            for i in xrange(num_processors):
                tasks.put(None)

            tasks.join()

            self.sim_results.append(simulations.SimulationData(self.sim_names, [output[0] for output in outputs],
                                                               [output[1] for output in outputs]))
            if save_nets:
                self.save_nets()
            else:
                return self.sim_results[-1]

        except KeyboardInterrupt:
            self.sim_results.append(simulations.SimulationData(self.sim_names, [output[0] for output in outputs],
                                                               [output[1] for output in outputs]))
            if save_nets:
                self.save_nets()
            else:
                return self.sim_results[-1]

    def _train_single(self, taskQueue, resultQueue):

        try:
            while True:
                print('In Worker')
                task = taskQueue.get()

                if task is None:
                    taskQueue.task_done()
                    print('Out of Worker')
                    break

                net_index = task[0]
                # dataset = self.train_set
                trainer = self.trainers[net_index]
                num_epochs = task[1]
                decimation = task[2]

                exec 'net = buildNetwork(%s)'%self._net_strings[net_index]

                net._setParameters(self._weights[net_index])


                trainer.module = net
                trainer.setData(self.train_set)


                train_err = []
                test_err = []
                for i in range(num_epochs):
                    res = trainer.train()


                    if i % decimation == 0:
                        print('%d Epochs Complete on Network %d' % (i, net_index))
                        predictions = [net.activate(val) for val in self.train_set['input']]
                        if self.custom_error:
                            train_err.append(self.custom_error(np.asarray(predictions), self.train_set['target']))
                        else:
                            train_err.append(np.sum(0.5*((self.train_set['target'] - predictions)**2)) /
                                             float(self.train_set.getLength()))

                        if self.test_set:
                            predictions = [net.activate(val) for val in self.test_set['input']]
                            if self.custom_error:
                                test_err.append(self.custom_error(np.asarray(predictions), self.test_set['target']))
                            else:
                                test_err.append(np.sum(0.5*((self.test_set['target'] - predictions)**2)) /
                                                float(self.test_set.getLength()))
                        else:
                            test_err = [None]
                    # print(res)

                print('\nTotal Epochs: %d' % trainer.totalepochs)
                print('Error: %0.5f' % res)

                #FIXME: add custom error functions and return test error as well
                #FIXME: make sure it works right for classification too.

                taskQueue.task_done()
                resultQueue.put((np.asarray(train_err), np.asarray(test_err)))

        except KeyboardInterrupt:
            resultQueue.put(KeyboardInterrupt)


if __name__ == '__main__':

    net_dict = dict(name='net1', input=3, hidden=[4, 4], output=2, outclass='LinearLayer')
    net_dict1 = dict(name='net2', input=3, hidden=40, output=2, outclass='LinearLayer')
    net_dict2 = dict(name='net3', input=3, hidden=[20, 8], output=2, outclass='LinearLayer')
    net_dict3 = dict(name='net4', input=3, hidden=10, output=2, outclass='LinearLayer')
    net_dict4 = dict(name='net5', input=3, hidden=[1, 4], output=2, outclass='LinearLayer')
    trainer_dict = dict(type='BackpropTrainer', learningrate=0.001)

    inputs = np.random.random((150,3))
    targets = np.random.random((150,2))

    test_inputs = inputs
    test_targets = targets

    inputs = dict(train=inputs, test=test_inputs)
    targets = dict(train=targets, test=test_targets)

    sim = Simulation(inputs, targets, [net_dict, net_dict1, net_dict2, net_dict3, net_dict4], dataset_properties=[],
                     trainers_properties=[trainer_dict, trainer_dict, trainer_dict, trainer_dict, trainer_dict])

    sim_results = sim.run(50, decimation=5)

    sim_results.plot_data()

