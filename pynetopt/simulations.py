__author__ = 'Jeff'

from pybrain import datasets
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.supervised.trainers import RPropMinusTrainer
from pybrain.structure import LinearLayer
from pybrain.structure import TanhLayer
from pybrain.structure import SigmoidLayer
from pybrain.structure import SoftmaxLayer

import matplotlib.pyplot as plt
import numpy as np

import Tkinter, tkFileDialog
import os
import re
import cPickle as pickle


class Simulation(object):
    """
    Parent object for all simulations.
    """

    #FIXME: Add crossvalidation options
    def __init__(self, inputs, targets, nets_properties, dataset_properties, trainers_properties, custom_error=None):

        self.nets_properties = nets_properties
        self._weights = []
        self._build_networks()

        assert type(inputs) == type(targets), 'inputs and targets must both be numpy array or dict with' \
                                              '"train" and "test" keys'

        if type(inputs) == dict:
            self.train_inputs = inputs['train']
            self.train_targets = targets['train']

            self.test_inputs = inputs['test']
            self.test_targets = targets['test']

        elif type(inputs) == np.ndarray:
            self.train_inputs = inputs
            self.train_targets = targets

            self.test_inputs = None
            self.test_targets = None

        self._build_datasets()


        self.trainers_properties = trainers_properties
        self._build_trainers()

        self.dataset_properties = dataset_properties
        self.custom_error = custom_error



        self.sim_results = []

        self.sim_names = [net['name'] for net in nets_properties]


    @property
    def nets(self):
        _, networks = self._build_networks()

        return networks

    # @property
    def _build_trainers(self):
        trainersList = []
        trainer = None
        trainer_nets = self.nets
        for trainer_dict, netObj in zip(self.trainers_properties, trainer_nets):

            train_string = 'netObj, dataset=self.train_set,  '

            if trainer_dict['type'] == 'BackpropTrainer':
                valid_keys = ['learningrate', 'lrdecay', 'momentum', 'verbose', 'batchlearning', 'weightdecay']
                for key in valid_keys:
                    try:
                        train_string += key + '=' + str(trainer_dict[key]) + ', '
                    except KeyError:
                        continue

                exec 'trainer = BackpropTrainer(%s)'%train_string
                trainersList.append(trainer)

            elif trainer_dict['type'] == 'RPropMinusTrainer':
                valid_keys = ['etaminus', 'etaplus', 'deltamin', 'deltamax', 'delta0']
                for key in valid_keys:
                    try:
                        train_string += key + '=' + str(trainer_dict[key]) + ', '
                    except KeyError:
                        continue

                exec 'trainer = RPropMinusTrainer(%s)'%train_string
                trainersList.append(trainer)

        self.trainers = tuple(trainersList)


    def run(self, epochs):
        print('If you are seeing this, you should be overwriting this method with your own class method!')


    def _build_networks(self):
        self._net_strings = []
        net = None
        # Check if nets have been built before
        if len(self._weights) == 0:
            new = True
        else:
            new = False

        nets = []
        for net_dict in self.nets_properties:
            #FIXME: Build out test cases

            net_string=''
            valid_keys = ['input', 'hidden', 'output', 'hiddenclass', 'outclass', 'bias']
            for key in valid_keys:
                try:
                    if key in ['input', 'output']:
                        net_string += str(net_dict[key]) + ', '

                    elif key == 'hidden':
                        val = net_dict[key]
                        if type(val) is list:
                            for layer in val:
                                net_string += str(layer) + ', '
                        else:
                            net_string += str(val) + ', '
                    else:
                        net_string += key + '=' + str(net_dict[key]) + ', '

                except KeyError:
                    continue

            #FIXME: This is the best way I could find to do this
            exec 'net = buildNetwork(%s)'%net_string
            nets.append(net)
            self._net_strings.append(net_string)

        if new:
            self._weights = tuple([network.params for network in nets])
            return self._weights, tuple(nets)

        else:
            for network, weights in zip(nets, self._weights):
                network._setParameters(weights)

            return self._weights, tuple(nets)

    def save_nets(self):
        root = Tkinter.Tk()
        root.withdraw()


        _, netList = self._build_networks()

        for net, name in zip(netList, self.sim_names):
            outPath = ''
            while len(outPath)<3:
                outPath = tkFileDialog.askdirectory(title='Pick an output directory for your %s.'%name)

            outPath = os.path.join(outPath, '%s_ANN.pynet'%name)

            outPath = re.sub(':','-',outPath)

            fobj = open(outPath,'wb')
            pickle.dump(net,fobj)
            fobj.close()

    def save_sim_results(self):
        pass

    def _build_datasets(self):
        self.train_set = datasets.SupervisedDataSet(self.train_inputs.shape[1], self.train_targets.shape[1])

        for input, target in zip(self.train_inputs, self.train_targets):
            self.train_set.addSample(input, target)

        if self.test_inputs is not None:
            self.test_set = datasets.SupervisedDataSet(self.test_inputs.shape[1], self.test_targets.shape[1])

            for input, target in zip(self.test_inputs, self.test_targets):
                self.test_set.addSample(input, target)
        else:
            self.test_set = None


class SimulationData(object):
    def __init__(self, net_names, train_results, test_results):
        self.net_names = net_names
        self.train_results = train_results
        # print(test_results)
        self.test_results = test_results
        # print(self.test_results-self.train_results)
        # print(self.test_results[0][0])

    def plot_data(self, title='',separate=False):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for name, results in zip(self.net_names, self.train_results):
            ax.plot(results, label=name)

        ax.legend(loc='best')
        ax.set_title(title+' Train Results')

        if self.test_results[0][0] is not None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for name, results in zip(self.net_names, self.test_results):
                ax.plot(results, label=name)

            ax.legend(loc='best')
            ax.set_title(title+' Test Results')

        plt.show()



if __name__ == '__main__':

    net_dict = dict(input=3, hidden=[4, 4], output=2, outclass='LinearLayer')

    Simulation([],[],[net_dict], dataset_properties=[], trainers_properties=[])._build_networks()